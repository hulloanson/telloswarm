from setuptools import setup

setup(name='telloswarm',
    version='0.1',
    description='The library to swarm Tello Edu drones with Python 3',
    url='https://gitlab.com/hulloanson/telloswarm.git',
    author='hulloanson',
    author_email='hulloanson@gmail.com',
    license='MIT',
    packages=['telloswarm'],
    install_requires=['netifaces', 'netaddr'],
    zip_safe=False,
)